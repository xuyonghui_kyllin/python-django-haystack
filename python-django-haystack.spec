%global _empty_manifest_terminate_build 0
Name:		python-django-haystack
Version:	3.3.0
Release:	1
Summary:	Pluggable search for Django.
License:	BSD
URL:		http://haystacksearch.org/
Source0:	https://files.pythonhosted.org/packages/b4/09/623634ca5f8b9fe99d07d284491724eb1b0704022e942a1fe815c1d13a02/django_haystack-3.3.0.tar.gz
BuildArch:	noarch

%description
Haystack provides modular search for Django. It features a unified, familiar API that allows you to plug in different search backends (such as Solr, Elasticsearch, Whoosh, Xapian, etc.) without having to modify your code.

%package -n python3-django-haystack
Summary:	Pluggable search for Django.
Provides:	python-django-haystack
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:  python3-setuptools_scm

Requires:       python3-django

%description -n python3-django-haystack
Haystack provides modular search for Django. It features a unified, familiar API that allows you to plug in different search backends (such as Solr, Elasticsearch, Whoosh, Xapian, etc.) without having to modify your code.

%package help
Summary:	Development documents and examples for django-haystack
Provides:	python3-django-haystack-doc
%description help
Development documents and examples for django-haystack.

%prep
%autosetup -n django-haystack-3.2.1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-django-haystack -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Oct 23 2024 xuyonghui <xuyonghui@kylinos.cn> - 3.3.0-1
- Update package to version 3.3.0

* Thu Jul 14 2022 wangshuo <wangshuo@kylinos.cn> - 3.2.1-1
- Package Spec generated
